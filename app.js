var express = require("express");
var app = express();
var serv = require('http').Server(app);

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/public/index/index.html');
});
app.use('/public', express.static(__dirname + '/public'));

serv.listen(3000);

console.log("Server started...");