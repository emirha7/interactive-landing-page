var animationEnded = false;

TweenMax.from('.intcannon-logo', 1, {
	delay: 1.0,
	opacity: 0,
	ease: Expo.easyInOut,
	onComplete: function() {
		bulletShot();
	}
});


TweenMax.from(".right", 1, {
	delay: 1,
	width: 0,
	ease: Expo.easyInOut,
	onComplete: function() {

	}
});

function bulletShot() {
	$(".logo-bullet").css("opacity", 1);
	TweenMax.to(".logo-bullet", 0.5, {
		delay: 0,
		left: '50vw',
		y: -40,
		ease: Power0.easeNone,
		onComplete: function() {
			setupParticlesJS_emir();
			animationEnded = true;
		}
	});
}

TweenMax.from(".watch", 1, {
	delay: 1,
	opacity: 0,
	y: -20,
	ease: Expo.easyInOut,
	onComplete: function() {

	}
});

TweenMax.from(".btn", 1, {
	delay: 1,
	opacity: 0,
	ease: Expo.easyInOut
});

TweenMax.from(".left-bottom", 1, {
	delay: 1,
	opacity: 0,
	x: -100,
	ease: Expo.easyInOut
});



TweenMax.from(".company-name", 1, {
	delay: 1,
	opacity: 0,
	y: -100,
	ease: Expo.easyInOut
});

TweenMax.from(".description", 1, {
	delay: 1,
	opacity: 0,
	x: -20,
	ease: Expo.easyInOut
});



/*function tankAnimation(){
	var tl = new TimelineMax({
		repeat:0,
		repeatDelay:0.5});
	tl.to($('.driving-tank'), 2, {css: {top: 490}});
	tl.to($('.driving-tank'), 1.5, {css:{rotation:-180}, onComplete: function(){

	}});

	tl.play();
}

tankAnimation();
*/

$("#watch-video-button").on("click", function() {
	$("#video-wrapper.outer").css("z-index", 1);
	TweenMax.to("#video-wrapper", 1, {
		width: "100%",
		height: "100%",
		zIndex: 1,
		ease: Expo.easyInOut,
		onComplete: function() {
			console.log("haha1!");
			videoAnimation();
		}
	});

	function videoAnimation() {
		TweenMax.to(".myvideo", 1, {
			width: "100%",
			height: "100%",
			zIndex: 12,
			ease: Expo.easyInOut,
			onComplete: function() {
				console.log("haha2!");
			}
		});
	}

});
