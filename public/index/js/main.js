var velocity = {
	x: 0,
	y: 0
}

var rotation = 0;
var turretRotation = 0;

var offsetX = 0;
var offsetY = 0;

var sideX = 0;
var sideY = 0;

var closest = null;
var closestDist = 9999999;

setInterval(function (){

	driveTank();

	closest = null;
	closestDist = 9999999;

	setHoverTank("GAMES", "neki", null);
	setHoverTank("WEB", "neki", null);
	setHoverTank("SOFTWARE", "neki", null);

	setHoverTank("left-game-box", "show-nav", "my-arrow-up");
	setHoverTank("right-game-box", "show-nav", "my-arrow-down");

	for (var i = 0; i < 6; i++) {
		setHoverTank("web-box" + i, "card-flip-force", null);
	}

	for (var i = 0; i < 2; i++) {
		setHoverTank("team-box" + i, "force-zoom", null);
	}

	for (var i = 0; i < 4; i++) {
		setHoverTank("g-box" + i, "force-game-zoom", null);
	}

	for (var i = 0; i < 3; i++) {
		setHoverTank("software-card" + i, "force-zoom", null);
	}

	rotateTurret(closest);

}, 16);

function rotateTurret(closest){

	let centerX = window.innerWidth/2 + offsetX;
	let centerY = window.innerHeight/2 + offsetY;

	let deltaX = centerX - (closest.left + closest.right)/2;
	let deltaY = centerY - (closest.top + closest.bottom)/2;

	let angle = Math.atan2(deltaY,deltaX)/Math.PI * 180 - 90;
	turretRotation = smoothRotate(turretRotation, angle);

	rotate($("#turret"),turretRotation);
}

function driveTank(){

	var elem = $("#container");
	var len = Math.sqrt(velocity.x * velocity.x + velocity.y * velocity.y);

	if(len > 10)
		len = 10;
	else if(len <= 0)
		len = 0.0001

	var vx = velocity.x/len;
	var vy = velocity.y/len;

	var angle = Math.atan2(vy,vx)/Math.PI * 180 - 90;
	rotation = smoothRotate(rotation, angle);

	rotate($("#tank"),rotation);

	if(len > 3){

		var dot = Math.abs((rotation - angle)%360)/180;

		var dirx = Math.sin(rotation/180 * Math.PI) * len * dot;
		var diry = -Math.cos(rotation/180 * Math.PI) * len * dot;

		if(sideY <= 0 && elem.offset().top >= window.innerHeight){
			elem.css('top', window.innerHeight);
			sideY -= diry;
			if(sideY < -window.innerHeight/2 + 50)
				sideY = -window.innerHeight/2  + 50;
		}
		else if(sideY >= 0 && elem.offset().top <= -window.innerHeight){
			elem.css('top', -window.innerHeight);
			sideY -= diry;
			if(sideY > window.innerHeight/2 - 50)
				sideY = window.innerHeight/2  - 50;
		}
		else{
			elem.css('top', elem.offset().top + diry);
			sideY = 0;
		}

		if(sideX <= 0 && elem.offset().left >= window.innerWidth){
			elem.css('left', window.innerWidth);
			sideX -= dirx;
			if(sideX < -window.innerWidth/2  + 50)
				sideX = -window.innerWidth/2  + 50;
		}
		else if(sideX >= 0 && elem.offset().left <= -window.innerWidth){
			elem.css('left', -window.innerWidth);
			sideX -= dirx;
			if(sideX > window.innerWidth/2 - 50)
				sideX = window.innerWidth/2  - 50;
		}
		else{
			elem.css('left', elem.offset().left + dirx);
			sideX = 0;
		}

		offsetX += dirx;
		offsetY += diry;
	}

	offsetX*=0.94;
	offsetY*=0.94;

	$(".driving-tank").css('left','calc(50% + '+(offsetX + sideX)+'px)');
	$(".driving-tank").css('top','calc(50% + '+(offsetY + sideY)+'px)');
}

function setHoverTank(string, classvar, string2) {

	let centerX = window.innerWidth / 2 + offsetX + sideX;
	let centerY = window.innerHeight / 2 + offsetY + sideY;

	let rekt = document.getElementById(string).getBoundingClientRect();

	let x = (rekt.left + rekt.right) / 2;
	let y = (rekt.top + rekt.bottom) / 2;

	let dist = Math.sqrt((x - centerX) * (x - centerX) + (y - centerY) * (y - centerY));

	if (dist < closestDist) {
		closestDist = dist;
		closest = rekt;
	}

	if (centerX > rekt.left - 90 && centerX < rekt.right + 90) {
		if (centerY > rekt.top - 90 && centerY < rekt.bottom + 90) {
			if (string2 != null) {
				$("#" + string2).addClass(classvar);
				//alert("string2 = " + string2 + " classvar = " + classvar);


			} else {
				$("#" + string).addClass(classvar)
			}
		} else {
			if (string2 != null) {
				$("#" + string2).addClass(classvar);
			} else {
				$("#" + string).removeClass(classvar)
			}
		}

	} else {
		if (string2 != null) {
			$("#" + string2).removeClass(classvar)
		} else {
			$("#" + string).removeClass(classvar)
		}
	}

}

function smoothRotate(rotation, angle){
	let delta = Math.abs(rotation-angle);
	if(delta > 2)
		delta = 2;

	let diff = (rotation - angle)%360;
	if (diff > 0)
		delta = -delta;

	if (Math.abs(diff) > 180)
		delta = -delta;

	let smooth = clamp(Math.abs(Math.abs(diff)-180)/5, 0, 1);
	return (rotation - delta*smooth)%360;
}

function clamp(val, from, to){
	if (val < from)
		val = from;
	else if (val > to)
		val = to;
	return val;
}

window.addEventListener("mousemove", function(e){
	if(!animationEnded)
		return;

	var centerX = window.innerWidth/2 + offsetX + sideX;
	var centerY = window.innerHeight/2 + offsetY + sideY;

	var deltaX = Math.abs(centerX - e.clientX)
	var deltaY = Math.abs(centerY - e.clientY)

	velocity.x = (centerX - e.clientX)*0.02;
	velocity.y = (centerY - e.clientY)*0.02;
});


function rotate(element, degrees) {
    element.css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
                 '-moz-transform' : 'rotate('+ degrees +'deg)',
                 '-ms-transform' : 'rotate('+ degrees +'deg)',
                 'transform' : 'rotate('+ degrees +'deg)'})
};
